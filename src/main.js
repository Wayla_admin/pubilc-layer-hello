import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

import Dialog from "./components/dialog/index";
//import Dialog from "vue-pubilc-layer"
Vue.use(Dialog, {});

new Vue({
  render: (h) => h(App),
}).$mount("#app");
