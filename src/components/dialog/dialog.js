import dialog_com from "./index.vue";
import html_com from "./htmlTemplate.vue"
import defaultOption from "./defaultOption";
function Dialog(options) {
  this.Vue = options.Vue;
  //合并选项
  this.option = { ...defaultOption, ...options };
  this.dialog_cache = new Object(); //对话框缓存
  this.com_cache = new Object(); //vue组件缓存;
  this.min_layer = new Object(); //
  this.layer_z = 200; //基准
}

//打开对话框
Dialog.prototype.open = function (options) {
  options = options || {};
  var option = { ...this.option, ...options };
  //是否为多弹框模式
  if (!option.multiple) {
    this.closeAll();
  }
  option.uuid = uuid();
  let Vue = this.Vue;
  var profile = Vue.extend(dialog_com);
  //解析弹框头部
  if (option.hasHead && option.customHead) {
    this.getHtml_Head(option);
  }
  this.getHtml_Body(option); // 解析弹框内容
  var mixin = option.mixin || {}; //混淆扩展
  const instance = new profile({
    propsData: option,
    mixins: [mixin],
  }).$mount();
  document.body.appendChild(instance.$el);
  instance.uuid = option.uuid;
  this.dialog_cache[instance.uuid] = instance;
  this.layer_z += 2;
  return instance; //返回实例
};
//关闭对话框
Dialog.prototype.close = function (instance) {
  if (!instance) return;
  document.body.removeChild(instance.$el);
  delete this.dialog_cache[instance.uuid];
  this.minusminNum(instance.uuid);
  instance.$destroy();
};

//关闭所有对话框
Dialog.prototype.closeAll = function () {
  for (let key in this.dialog_cache) {
    var instance = this.dialog_cache[key];
    this.close(instance);
  }
  this.min_layer = new Object();
  this.layer_z = 200; //基准
};

//获取当前对话框个数
Dialog.prototype.getNums = function () {
  return Object.keys(this.dialog_cache).length;
};

Dialog.prototype.getHtml_Head = function (option) {
  option.HeadCom = this.getComName(option.customHead);
};

Dialog.prototype.getHtml_Body = function (option) {
  option.bodyCom = this.getComName(option.content);
};

//获取组件名称
Dialog.prototype.getComName = function (custom) {
  let Vue = this.Vue;
  var err = null;
  var key = null;
  switch (custom.type) {
    case "html":
      if (typeof custom.subCom !== "string") {
        err = "html类型应为html代码片段,请检查";
      }
      var html = custom.subCom;
      // key = "html-" + uuid();
      // Vue.component(key, {
      //   render(h) {
      //     return <div domPropsInnerHTML={html}></div>;
      //   },
      // });
      key="html-htmlTemplate";
      var has = this.com_cache[key];
      if (!has) {
        Vue.component(key, html_com);
        this.com_cache[key] = html_com;
      }
      custom.subData=html;
      break;
    case "vue":
      if (!isVueCom(custom.subCom)) {
        err = "vue类型组件对象不正确,请检查";
      }
      custom.subCom.__file=custom.subCom.__file|| uuid();
      key = "vue-" + custom.subCom.__file.split("/").join("-"); //唯一标识
      var has = this.com_cache[key];
      if (!has) {
        Vue.component(key, custom.subCom);
        this.com_cache[key] = custom.subCom;
      }
      break;
    case "dom":
      if (!(custom.subCom instanceof HTMLElement)) {
        err = "dom类型对象不正确,请检查";
      }
      var html = custom.subCom.outerHTML;
      // key = "dom-" + uuid();
      // Vue.component(key, {
      //   render(h) {
      //     return <div domPropsInnerHTML={html}></div>;
      //   },
      // });
      key="dom-htmlTemplate";
      var has = this.com_cache[key];
      if (!has) {
        Vue.component(key, html_com);
        this.com_cache[key] = html_com;
      }
      custom.subData=html;
      break;
    default:
      break;
  }

  err && console.error(err);
  return key;
};

Dialog.prototype.getminNum = function () {
  return Object.keys(this.min_layer).length;
};
Dialog.prototype.addminNum = function (key, layer) {
  this.min_layer[key] = layer;
};
Dialog.prototype.minusminNum = function (key) {
  delete this.min_layer[key];
};
Dialog.prototype.dragBind = function (dragBox, moveBox = dragBox) {
  Dialog.dragBind(dragBox, moveBox);
  dragBox.style.cursor = "move";
};
Dialog.dragBind = function (dragBox, moveBox = dragBox) {
  dragBox.onmousedown = (e) => {
    var disX = e.clientX - moveBox.offsetLeft;
    var disY = e.clientY - moveBox.offsetTop;
    document.onmousemove = (e) => {
      e.preventDefault();
      var l = e.clientX - disX;
      var t = e.clientY - disY;
      var x = document.documentElement.clientWidth - moveBox.offsetWidth;
      var y = document.documentElement.clientHeight - moveBox.offsetHeight;
      l = l < 0 ? 0 : l > x ? x : l;
      t = t < 0 ? 0 : t > y ? y : t;
      moveBox.style.left = l < 0 ? 0 : l + "px";
      moveBox.style.top = t < 0 ? 0 : t + "px";
      return false;
    };
    document.onmouseup = () => {
      document.onmousemove = null;
      document.onmouseup = null;
      return false;
    };
    return false;
  };
};

export default Dialog;

//唯一标识
function uuid() {
  var temp_url = URL.createObjectURL(new Blob());
  var uuid = temp_url.toString();
  URL.revokeObjectURL(temp_url);
  return uuid.substr(uuid.lastIndexOf("/") + 1);
}

//判断是否为vue对象
function isVueCom(vm) {
  return vm && vm._compiled;
}
