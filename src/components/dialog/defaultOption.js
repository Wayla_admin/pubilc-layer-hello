//对话框配置
//默认选项
//类型传递
//（支持vue组件、html代码片段、dom元素）
// Class dialogCom  {
//   type:vue、html、dom、
//   subCom:{}
//   subData：{}
// }
var defaultOption = {
  hasHead: true, //是否拥有头部(完成)
  customHead: null, //自定义头部内容,见Class dialogCom ，hasHead为true生效(完成)
  title: "标题", //标题(完成)
  hasMin: true, //是否有最小化按钮(完成)
  hasMax: true, //是否有最大化按钮(完成)
  content: null, //弹框内容,见Class dialogCom (完成)
  move: false, //是否可以拖动(完成)
  shade: false, //是否有遮罩(完成)
  blank: false, //是否在点击空白处自动关闭弹框，仅在shade为true时有效(完成)
  minWidth: 300, //最小化宽度(完成)
  minHight: 40, //最小化高度(完成)
  bottomOff: 30, //最小化后距离窗口底部距离(完成)
  leftOff: 4, //最小化后距离窗口左边距离(完成)
  rowGap: 8, //行间距(完成)
  colGap: 8, //列间距(完成)
  location: {
    //弹框位置，可由对象指定,为空则默认居中显示(完成)
    top: null,
    left: null,
    bottom: null,
    right: null,
  },
  area: { //弹框大小(完成)
    height: "auto",
    width: "auto",
  },
  multiple: true, //是否为多弹框模式(完成)
  theme: null, //自定义皮肤,目前内置皮肤theme-lan、theme-lv(完成)
  //回调开始
  success: null, //弹出成功后回调,type: Function(完成)
  cancel: null, //弹框关闭回调,type: Function(完成)
  minBack: null, //最小化回调,type: Function(完成)
  maxBack: null, //最大化回调,type: Function(完成)
  restore: null, //复原后回调,type: Function(完成)
  mixin: null, //混淆，用于完全个性化制定(完成)
};

export default defaultOption;
