
import Dialog from "./dialog";
import "./theme/index.scss";

var dialog={
    install: function(Vue, options) {
        options = options || {};
        options.Vue=Vue;
        options.name=options.name || "$dialog";
        var layer=new Dialog(options);
        Vue.prototype[options.name] = layer; //挂在到vue原型
        if(window.Vue){
            Vue.prototype[options.name] = layer; //挂在到vue原型
        }
    }
};


export default dialog;